---
layout: page
title: Sobre
permalink: /sobre/
feature-img: 
tags: [Sobre, Archive]
---

>  O projeto Transformação Digital do Governo do Ceará espera proprorcionar a interação entre o povo Cearence e o Governo por meio de canais digitais, como como sites, sistemas, e-mail e aplicativos móveis. Ao construir serviços digitais que atendam às suas necessidades, podemos tornar a entrega de nossa política e programas mais eficazes.

>> Os serviços digitais oferecem para a população a viabilidade de interagir com o governo e suas secretarias através de websites, emails e aplicativos móveis. Ao criar serviços digitais que atendam às necessidades dos cidadãos, é possível tornar mais eficaz a entrega de políticas e programas públicos. O Playbook - Serviços Digitais é um manual de 13 “peças-chave” extraídas de práticas bem-sucedidas do setor privado e do governo que, se seguidas juntas, ajudarão o governo a criar serviços digitais eficazes.
 
